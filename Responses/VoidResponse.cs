﻿using UnityEngine;
using UnityEngine.Events;

namespace GameEvents
{
    [System.Serializable]
    public class VoidResponse : UnityEvent<Void> { }
}
