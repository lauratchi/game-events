﻿using UnityEngine;

namespace GameEvents
{
    public class Vector3EventListener : BaseGameEventListener<Vector3, Vector3Event, Vector3Response> { }
}
