﻿using System.Collections.Generic;
using UnityEngine;

namespace GameEvents
{
    public abstract class BaseGameEvent<T> : ScriptableObject
    {
        private List<IGameEventListener<T>> listeners = new List<IGameEventListener<T>>();

        public void Raise(T parameter)
        {
            for(int i = 0; i < listeners.Count; i++)
            {
                listeners[i].OnEventRaised(parameter);
            }
        }

        public void RegisterListener(IGameEventListener<T> listener)
        {
            if (!listeners.Contains(listener))
            {
                listeners.Add(listener);
            }
        }

        public void UnregisterListener(IGameEventListener<T> listener)
        {
            if (listeners.Contains(listener))
            {
                listeners.Remove(listener);
            }
        }
    }
}
